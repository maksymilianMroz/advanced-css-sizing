# Zaawansowane techniki wymiarowania w CSS
<br>

W CSS mamy wiele zaawansowanych technik wymiarowania elementów. Oprócz podstawowych jednostek takich jak piksele (```px```) czy procenty (```%```), istnieją również funkcje pozwalające na dynamiczne dostosowywanie rozmiaru elementu do zmieniającej się zawartości lub parametrów okna przeglądarki.
<br><br>
___

<br><br>
### **min():** 

Funkcja ```min()``` pozwala określić minimalną wartość z dwóch podanych wartości. Na przykład, jeśli chcesz ustawić minimalną szerokość elementu na 500px pikseli lub 50% szerokości widoku, możesz użyć tej funkcji w ten sposób:

```css 
.min {
    width: min(500px, 50%);
} 
```

W tym przypadku szerokość elementu wynosić będzie 500px, jeśli 500px jest mniejsze niż 50% szerokości widoku, a szerokość elementu będzie wynosić 50%, jeśli 50% szerokości widoku jest mniejsze niż 500px.

<br><br>
___

<br><br>

### **max():** 

Funkcja `max()` pozwala określić maksymalną wartość z dwóch podanych wartości. Na przykład, jeśli chcesz ustawić maksymalną szerokość elementu na 500px pikseli lub 50% szerokości widoku, możesz użyć tej funkcji w ten sposób:

```css 
.max {
    width: max(500px, 50%);
} 
```
Czyli funkcja max działa na odwrót do funkcji min - wybieramy z podanych parametrów wartość większą.

<br><br>
___

<br><br>

### **minmax():** 

Służy do określania przedziału (zakresu) dla wartości właściwości. Przyjmuje ona dwa argumenty - minimalną i maksymalną wartość, które mogą być wykorzystane dla danego elementu.

```css 
.container {
    grid-template-columns: repeat(auto-fit, minmax(300px, 400px));
} 
```
jeśli chcemy, aby kolumny były szerokie od 300px do 400px (proporcja dostosowująca się do dostępnego miejsca)

<br><br>
___
<br><br>

## **clamp()**
Funkcja `clamp()` umożliwia definiowanie elastycznej wartości pomiędzy minimum i maksimum. Ma trzy argumenty: wartość minimalna, preferowana (docelowa) wartość oraz wartość maksymalna. Przykład:
```css 
.clamp {
    width: clamp(200px, 50vw, 400px);
} 
```
W tym przypadku szerokość elementu będzie wynosić 50vw szerokości widoku, jeśli szerokość widoku jest większa/równa niż 200px a mniejsza/bądź równa niż 400px.

<br><br>

___

<br><br>
## **min-content** 

`min-content` to właściwość pozwalająca na ustawienie minimalnej szerokości elementu na podstawie jego zawartości. 
```css
.min-content {
    width: min-content;
}
```
Szerokość elementu będzie wynosiła tyle, ile jest potrzebne do wyświetlenia jego zawartości.

<br><br>

___

<br><br>

## **max-content** 

`max-content` ustawia szerokość elementu na maksymalną możliwą szerokość zawartości elementu.

```css 
.max-content {
    width: max-content;
}
```
<br><br>

___

<br><br>

## **fit-content**
`fit-content` umożliwia określenie rozmiaru elementu na podstawie jego zawartości. Jeśli chcesz ustawić minimalną szerokość elementu na szerokość jego zawartości, ale jednocześnie zadbać o to by przy małej szerokości widoku strona nie rozciągała się możesz użyć:
```css 
.fit-content {
    width: fit-content;
} 
```

<br><br>
___

<br><br>

## **calc()**
Umożliwia wykonywanie prostych obliczeń matematycznych wewnątrz deklaracji stylu, co pozwala na dynamiczne określanie wartości właściwości CSS.
```css 
.calc {
    width: calc(100% - 20px);
} 
```
Funkcja `calc()` przyjmuje wyrażenie matematyczne w nawiasach, np. calc(100% - 20px). Wewnątrz wyrażenia matematycznego można używać jednostek, takich jak piksele, em, rem, procenty, itp. oraz operatorów matematycznych, takich jak +, -, *, /.

<br><br>


___

<br><br>

# **Dodatkowa wtyczka**
Podczas zajęć padło też pytanie o to w jaki sposób moje pliki .css są formatowane tak, że znaki `:` są idealnie pod sobą:
```css 
.header {
    font-size       : 5rem;
    line-height     : 1;
    color           : #fff;
    padding         : 0.125em 0.5em;
    margin-bottom   : 1em;
    background-color: rgb(38, 166, 96);
}
```
Odpowiada za to wtyczka do Visual Studio Code, która nazywa się **formate: CSS/LESS/SCSS formatter** żeby łatwiej było znaleźć dodatkowe informacje: **v1.2.1** - twórcą jest **MikeBovenlander** - pobrań na ten moment: **172,600** - opis wtyczki: Formate is an CSS/LESS/SCSS format extension to format properties and align property values to improve readability.

<br><br>